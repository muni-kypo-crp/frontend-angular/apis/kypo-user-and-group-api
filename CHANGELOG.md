### 18.0.0 Update to Angular 18.
* 031f0c5 -- [CI/CD] Update packages.json version based on GitLab tag.
* 9daf02f -- Merge branch 'develop' into 'master'
* c7cc780 -- Change husky commit check to ng lint
* 2c5e0e1 -- Merge branch 'master' into develop
* b7ffcb8 -- Merge branch '28-update-to-angular-18' into 'develop'
* cc17bac -- Fix husky config
* 370ab84 -- Merge branch 'develop' into 28-update-to-angular-18
* 404fe74 -- Update VERSION.txt
* f738af4 -- Update pipeline to Angular 18
* 3e007ac -- Fix pipeline
* 4a9937e -- Update to angular 18
### 16.0.2 Integrate endpoint for roles not already assigned to a group.
* dd6179c -- [CI/CD] Update packages.json version based on GitLab tag.
* 28ad294 -- Merge branch 'develop' into 'master'
* 4d8160e -- Merge branch '27-add-endpoint-for-roles-not-already-assigned-to-group' into 'develop'
* 1ca4de1 -- Integrate endpoint for roles not already assigned to a group
### 16.0.1 Update sentinel version.
* 5368fa9 -- [CI/CD] Update packages.json version based on GitLab tag.
* aea7f5d -- Merge branch 'develop' into 'master'
* 0f6d99e -- Merge branch 'update-sentinel-versions' into 'develop'
* f3b8dfa -- Update sentinel version
### 16.0.0 Update to Angular 16.
* 3c18b1f -- [CI/CD] Update packages.json version based on GitLab tag.
*   d01a366 -- Merge branch '26-update-to-angular-16' into 'master'
|\  
| * 8047644 -- Update to Angular 16
|/  
* ae4308d -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 29dcde2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   94a1742 -- Merge branch '25-update-to-angular-15' into 'master'
|\  
| * b7e7d6a -- Update to angular 15
|/  
* 22da769 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f4e2cbc -- [CI/CD] Update packages.json version based on GitLab tag.
*   120ec0c -- Merge branch '24-update-to-angular-14' into 'master'
|\  
| * b5a0059 -- Resolve "Update to Angular 14"
|/  
* 0a44766 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a7fe0b3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4851855 -- Merge branch '23-add-method-to-import-users-from-file' into 'master'
|\  
| * 10805f0 -- Added method to import users from file
|/  
* 1e680c5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 25e24e6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   fd0d064 -- Merge branch '22-fix-getlocaloidcusers-error-parsing-from-blob' into 'master'
|\  
| * 80098fd -- Resolve "Fix getLocalOIDCUsers error parsing from blob"
|/  
* c7c05f5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 590552f -- [CI/CD] Update packages.json version based on GitLab tag.
*   6bbe54f -- Merge branch '21-add-local-oidc-download-endpoint' into 'master'
|\  
| * f427c5a -- Resolve "Add local OIDC download endpoint"
|/  
* 9934b09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 601d967 -- [CI/CD] Update packages.json version based on GitLab tag.
*   46253f1 -- Merge branch '20-update-to-angular-13' into 'master'
|\  
| * 17e4107 -- Resolve "Update to Angular 13"
|/  
*   61fdbb5 -- Merge branch '19-add-license-file' into 'master'
|\  
| * 7dcacbd -- Add license file
|/  
* deb2bd6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c7ca261 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3b6874d -- Merge branch '18-fix-mapping-of-the-roles-if-roles-are-undefined' into 'master'
|\  
| * 2cdc07f -- Resolve "Fix mapping of the roles if roles are undefined"
|/  
*   4f3d64f -- Merge branch '17-add-node-modules-to-gitignore' into 'master'
|\  
| * 81a14cc -- Add node_modules to gitignore
|/  
*   a8672f5 -- Merge branch '16-bump-version-of-sentinel' into 'master'
|\  
| * 4e99280 -- Bump version of Sentinel
|/  
* 26898fd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 19212ed -- [CI/CD] Update packages.json version based on GitLab tag.
*   f306a5a -- Merge branch '15-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 2d3d127 -- Update gitlab CI
|/  
* a85f65d -- Update project package.json version based on GitLab tag. Done by CI
*   27473c3 -- Merge branch '14-update-to-angular-12' into 'master'
|\  
| * 73f5f06 -- Resolve "Update to Angular 12"
|/  
* ccb5710 -- Update project package.json version based on GitLab tag. Done by CI
*   3a7ef81 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * fec892d -- peerDependency update
|/  
* ebc8fdb -- Update project package.json version based on GitLab tag. Done by CI
*   4b15cb4 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 7b4051c -- Update to Angular 11 and dependency update
|/  
*   5feb4cd -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 3ca700e -- recreate package lock
|/  
* 10d0e6b -- Update project package.json version based on GitLab tag. Done by CI
*   be9c86a -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * d4e076f -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   3622129 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 2b504ea -- Resolve "Migrate from tslint to eslint"
|/  
* a7d6ee1 -- Update project package.json version based on GitLab tag. Done by CI
*   ffea373 -- Merge branch '8-update-dependencies-to-new-format' into 'master'
|\  
| * 3d77a3a -- Update dependencies
|/  
* 0d62ecb -- Update project package.json version based on GitLab tag. Done by CI
*   af267dc -- Merge branch '7-rename-to-kypo-user-and-group-api' into 'master'
|\  
| * cad959d -- Rename package
|/  
*   2ec7fdb -- Merge branch '6-use-cypress-image-in-ci' into 'master'
|\  
| * f1a75eb -- Resolve "Use cypress image in CI"
|/  
* 08f0d63 -- Update project package.json version based on GitLab tag. Done by CI
*   3f10dda -- Merge branch '5-refactor-to-use-sentinel-common' into 'master'
|\  
| * 4329cf9 -- Refactor to use @sentinel/common instead of kypo-common
|/  
* 7265560 -- Update project package.json version based on GitLab tag. Done by CI
*   a951a9f -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 98c639d -- Resolve "Update to Angular 10"
|/  
*   36a3ad9 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 167ccc6 -- Update .gitlab-ci.yml
|/  
* 381de6e -- Merge branch '2-add-no-unused-vars-compiler-rule' into 'master'
* 30d1974 -- Add compiler role to throw err on unused local or parameters
### 15.0.0 Update to Angular 15.
* 29dcde2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   94a1742 -- Merge branch '25-update-to-angular-15' into 'master'
|\  
| * b7e7d6a -- Update to angular 15
|/  
* 22da769 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f4e2cbc -- [CI/CD] Update packages.json version based on GitLab tag.
*   120ec0c -- Merge branch '24-update-to-angular-14' into 'master'
|\  
| * b5a0059 -- Resolve "Update to Angular 14"
|/  
* 0a44766 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a7fe0b3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4851855 -- Merge branch '23-add-method-to-import-users-from-file' into 'master'
|\  
| * 10805f0 -- Added method to import users from file
|/  
* 1e680c5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 25e24e6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   fd0d064 -- Merge branch '22-fix-getlocaloidcusers-error-parsing-from-blob' into 'master'
|\  
| * 80098fd -- Resolve "Fix getLocalOIDCUsers error parsing from blob"
|/  
* c7c05f5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 590552f -- [CI/CD] Update packages.json version based on GitLab tag.
*   6bbe54f -- Merge branch '21-add-local-oidc-download-endpoint' into 'master'
|\  
| * f427c5a -- Resolve "Add local OIDC download endpoint"
|/  
* 9934b09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 601d967 -- [CI/CD] Update packages.json version based on GitLab tag.
*   46253f1 -- Merge branch '20-update-to-angular-13' into 'master'
|\  
| * 17e4107 -- Resolve "Update to Angular 13"
|/  
*   61fdbb5 -- Merge branch '19-add-license-file' into 'master'
|\  
| * 7dcacbd -- Add license file
|/  
* deb2bd6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c7ca261 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3b6874d -- Merge branch '18-fix-mapping-of-the-roles-if-roles-are-undefined' into 'master'
|\  
| * 2cdc07f -- Resolve "Fix mapping of the roles if roles are undefined"
|/  
*   4f3d64f -- Merge branch '17-add-node-modules-to-gitignore' into 'master'
|\  
| * 81a14cc -- Add node_modules to gitignore
|/  
*   a8672f5 -- Merge branch '16-bump-version-of-sentinel' into 'master'
|\  
| * 4e99280 -- Bump version of Sentinel
|/  
* 26898fd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 19212ed -- [CI/CD] Update packages.json version based on GitLab tag.
*   f306a5a -- Merge branch '15-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 2d3d127 -- Update gitlab CI
|/  
* a85f65d -- Update project package.json version based on GitLab tag. Done by CI
*   27473c3 -- Merge branch '14-update-to-angular-12' into 'master'
|\  
| * 73f5f06 -- Resolve "Update to Angular 12"
|/  
* ccb5710 -- Update project package.json version based on GitLab tag. Done by CI
*   3a7ef81 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * fec892d -- peerDependency update
|/  
* ebc8fdb -- Update project package.json version based on GitLab tag. Done by CI
*   4b15cb4 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 7b4051c -- Update to Angular 11 and dependency update
|/  
*   5feb4cd -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 3ca700e -- recreate package lock
|/  
* 10d0e6b -- Update project package.json version based on GitLab tag. Done by CI
*   be9c86a -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * d4e076f -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   3622129 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 2b504ea -- Resolve "Migrate from tslint to eslint"
|/  
* a7d6ee1 -- Update project package.json version based on GitLab tag. Done by CI
*   ffea373 -- Merge branch '8-update-dependencies-to-new-format' into 'master'
|\  
| * 3d77a3a -- Update dependencies
|/  
* 0d62ecb -- Update project package.json version based on GitLab tag. Done by CI
*   af267dc -- Merge branch '7-rename-to-kypo-user-and-group-api' into 'master'
|\  
| * cad959d -- Rename package
|/  
*   2ec7fdb -- Merge branch '6-use-cypress-image-in-ci' into 'master'
|\  
| * f1a75eb -- Resolve "Use cypress image in CI"
|/  
* 08f0d63 -- Update project package.json version based on GitLab tag. Done by CI
*   3f10dda -- Merge branch '5-refactor-to-use-sentinel-common' into 'master'
|\  
| * 4329cf9 -- Refactor to use @sentinel/common instead of kypo-common
|/  
* 7265560 -- Update project package.json version based on GitLab tag. Done by CI
*   a951a9f -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 98c639d -- Resolve "Update to Angular 10"
|/  
*   36a3ad9 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 167ccc6 -- Update .gitlab-ci.yml
|/  
* 381de6e -- Merge branch '2-add-no-unused-vars-compiler-rule' into 'master'
* 30d1974 -- Add compiler role to throw err on unused local or parameters
### 14.0.0 Update to Angular 14.
* f4e2cbc -- [CI/CD] Update packages.json version based on GitLab tag.
*   120ec0c -- Merge branch '24-update-to-angular-14' into 'master'
|\  
| * b5a0059 -- Resolve "Update to Angular 14"
|/  
* 0a44766 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a7fe0b3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4851855 -- Merge branch '23-add-method-to-import-users-from-file' into 'master'
|\  
| * 10805f0 -- Added method to import users from file
|/  
* 1e680c5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 25e24e6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   fd0d064 -- Merge branch '22-fix-getlocaloidcusers-error-parsing-from-blob' into 'master'
|\  
| * 80098fd -- Resolve "Fix getLocalOIDCUsers error parsing from blob"
|/  
* c7c05f5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 590552f -- [CI/CD] Update packages.json version based on GitLab tag.
*   6bbe54f -- Merge branch '21-add-local-oidc-download-endpoint' into 'master'
|\  
| * f427c5a -- Resolve "Add local OIDC download endpoint"
|/  
* 9934b09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 601d967 -- [CI/CD] Update packages.json version based on GitLab tag.
*   46253f1 -- Merge branch '20-update-to-angular-13' into 'master'
|\  
| * 17e4107 -- Resolve "Update to Angular 13"
|/  
*   61fdbb5 -- Merge branch '19-add-license-file' into 'master'
|\  
| * 7dcacbd -- Add license file
|/  
* deb2bd6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c7ca261 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3b6874d -- Merge branch '18-fix-mapping-of-the-roles-if-roles-are-undefined' into 'master'
|\  
| * 2cdc07f -- Resolve "Fix mapping of the roles if roles are undefined"
|/  
*   4f3d64f -- Merge branch '17-add-node-modules-to-gitignore' into 'master'
|\  
| * 81a14cc -- Add node_modules to gitignore
|/  
*   a8672f5 -- Merge branch '16-bump-version-of-sentinel' into 'master'
|\  
| * 4e99280 -- Bump version of Sentinel
|/  
* 26898fd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 19212ed -- [CI/CD] Update packages.json version based on GitLab tag.
*   f306a5a -- Merge branch '15-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 2d3d127 -- Update gitlab CI
|/  
* a85f65d -- Update project package.json version based on GitLab tag. Done by CI
*   27473c3 -- Merge branch '14-update-to-angular-12' into 'master'
|\  
| * 73f5f06 -- Resolve "Update to Angular 12"
|/  
* ccb5710 -- Update project package.json version based on GitLab tag. Done by CI
*   3a7ef81 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * fec892d -- peerDependency update
|/  
* ebc8fdb -- Update project package.json version based on GitLab tag. Done by CI
*   4b15cb4 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 7b4051c -- Update to Angular 11 and dependency update
|/  
*   5feb4cd -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 3ca700e -- recreate package lock
|/  
* 10d0e6b -- Update project package.json version based on GitLab tag. Done by CI
*   be9c86a -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * d4e076f -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   3622129 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 2b504ea -- Resolve "Migrate from tslint to eslint"
|/  
* a7d6ee1 -- Update project package.json version based on GitLab tag. Done by CI
*   ffea373 -- Merge branch '8-update-dependencies-to-new-format' into 'master'
|\  
| * 3d77a3a -- Update dependencies
|/  
* 0d62ecb -- Update project package.json version based on GitLab tag. Done by CI
*   af267dc -- Merge branch '7-rename-to-kypo-user-and-group-api' into 'master'
|\  
| * cad959d -- Rename package
|/  
*   2ec7fdb -- Merge branch '6-use-cypress-image-in-ci' into 'master'
|\  
| * f1a75eb -- Resolve "Use cypress image in CI"
|/  
* 08f0d63 -- Update project package.json version based on GitLab tag. Done by CI
*   3f10dda -- Merge branch '5-refactor-to-use-sentinel-common' into 'master'
|\  
| * 4329cf9 -- Refactor to use @sentinel/common instead of kypo-common
|/  
* 7265560 -- Update project package.json version based on GitLab tag. Done by CI
*   a951a9f -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 98c639d -- Resolve "Update to Angular 10"
|/  
*   36a3ad9 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 167ccc6 -- Update .gitlab-ci.yml
|/  
* 381de6e -- Merge branch '2-add-no-unused-vars-compiler-rule' into 'master'
* 30d1974 -- Add compiler role to throw err on unused local or parameters
### 13.0.3 Add method to support import of users
* a7fe0b3 -- [CI/CD] Update packages.json version based on GitLab tag.
*   4851855 -- Merge branch '23-add-method-to-import-users-from-file' into 'master'
|\  
| * 10805f0 -- Added method to import users from file
|/  
* 1e680c5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 25e24e6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   fd0d064 -- Merge branch '22-fix-getlocaloidcusers-error-parsing-from-blob' into 'master'
|\  
| * 80098fd -- Resolve "Fix getLocalOIDCUsers error parsing from blob"
|/  
* c7c05f5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 590552f -- [CI/CD] Update packages.json version based on GitLab tag.
*   6bbe54f -- Merge branch '21-add-local-oidc-download-endpoint' into 'master'
|\  
| * f427c5a -- Resolve "Add local OIDC download endpoint"
|/  
* 9934b09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 601d967 -- [CI/CD] Update packages.json version based on GitLab tag.
*   46253f1 -- Merge branch '20-update-to-angular-13' into 'master'
|\  
| * 17e4107 -- Resolve "Update to Angular 13"
|/  
*   61fdbb5 -- Merge branch '19-add-license-file' into 'master'
|\  
| * 7dcacbd -- Add license file
|/  
* deb2bd6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c7ca261 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3b6874d -- Merge branch '18-fix-mapping-of-the-roles-if-roles-are-undefined' into 'master'
|\  
| * 2cdc07f -- Resolve "Fix mapping of the roles if roles are undefined"
|/  
*   4f3d64f -- Merge branch '17-add-node-modules-to-gitignore' into 'master'
|\  
| * 81a14cc -- Add node_modules to gitignore
|/  
*   a8672f5 -- Merge branch '16-bump-version-of-sentinel' into 'master'
|\  
| * 4e99280 -- Bump version of Sentinel
|/  
* 26898fd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 19212ed -- [CI/CD] Update packages.json version based on GitLab tag.
*   f306a5a -- Merge branch '15-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 2d3d127 -- Update gitlab CI
|/  
* a85f65d -- Update project package.json version based on GitLab tag. Done by CI
*   27473c3 -- Merge branch '14-update-to-angular-12' into 'master'
|\  
| * 73f5f06 -- Resolve "Update to Angular 12"
|/  
* ccb5710 -- Update project package.json version based on GitLab tag. Done by CI
*   3a7ef81 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * fec892d -- peerDependency update
|/  
* ebc8fdb -- Update project package.json version based on GitLab tag. Done by CI
*   4b15cb4 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 7b4051c -- Update to Angular 11 and dependency update
|/  
*   5feb4cd -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 3ca700e -- recreate package lock
|/  
* 10d0e6b -- Update project package.json version based on GitLab tag. Done by CI
*   be9c86a -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * d4e076f -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   3622129 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 2b504ea -- Resolve "Migrate from tslint to eslint"
|/  
* a7d6ee1 -- Update project package.json version based on GitLab tag. Done by CI
*   ffea373 -- Merge branch '8-update-dependencies-to-new-format' into 'master'
|\  
| * 3d77a3a -- Update dependencies
|/  
* 0d62ecb -- Update project package.json version based on GitLab tag. Done by CI
*   af267dc -- Merge branch '7-rename-to-kypo-user-and-group-api' into 'master'
|\  
| * cad959d -- Rename package
|/  
*   2ec7fdb -- Merge branch '6-use-cypress-image-in-ci' into 'master'
|\  
| * f1a75eb -- Resolve "Use cypress image in CI"
|/  
* 08f0d63 -- Update project package.json version based on GitLab tag. Done by CI
*   3f10dda -- Merge branch '5-refactor-to-use-sentinel-common' into 'master'
|\  
| * 4329cf9 -- Refactor to use @sentinel/common instead of kypo-common
|/  
* 7265560 -- Update project package.json version based on GitLab tag. Done by CI
*   a951a9f -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 98c639d -- Resolve "Update to Angular 10"
|/  
*   36a3ad9 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 167ccc6 -- Update .gitlab-ci.yml
|/  
* 381de6e -- Merge branch '2-add-no-unused-vars-compiler-rule' into 'master'
* 30d1974 -- Add compiler role to throw err on unused local or parameters
### 13.0.2 Add json error converter
* 25e24e6 -- [CI/CD] Update packages.json version based on GitLab tag.
*   fd0d064 -- Merge branch '22-fix-getlocaloidcusers-error-parsing-from-blob' into 'master'
|\  
| * 80098fd -- Resolve "Fix getLocalOIDCUsers error parsing from blob"
|/  
* c7c05f5 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 590552f -- [CI/CD] Update packages.json version based on GitLab tag.
*   6bbe54f -- Merge branch '21-add-local-oidc-download-endpoint' into 'master'
|\  
| * f427c5a -- Resolve "Add local OIDC download endpoint"
|/  
* 9934b09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 601d967 -- [CI/CD] Update packages.json version based on GitLab tag.
*   46253f1 -- Merge branch '20-update-to-angular-13' into 'master'
|\  
| * 17e4107 -- Resolve "Update to Angular 13"
|/  
*   61fdbb5 -- Merge branch '19-add-license-file' into 'master'
|\  
| * 7dcacbd -- Add license file
|/  
* deb2bd6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c7ca261 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3b6874d -- Merge branch '18-fix-mapping-of-the-roles-if-roles-are-undefined' into 'master'
|\  
| * 2cdc07f -- Resolve "Fix mapping of the roles if roles are undefined"
|/  
*   4f3d64f -- Merge branch '17-add-node-modules-to-gitignore' into 'master'
|\  
| * 81a14cc -- Add node_modules to gitignore
|/  
*   a8672f5 -- Merge branch '16-bump-version-of-sentinel' into 'master'
|\  
| * 4e99280 -- Bump version of Sentinel
|/  
* 26898fd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 19212ed -- [CI/CD] Update packages.json version based on GitLab tag.
*   f306a5a -- Merge branch '15-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 2d3d127 -- Update gitlab CI
|/  
* a85f65d -- Update project package.json version based on GitLab tag. Done by CI
*   27473c3 -- Merge branch '14-update-to-angular-12' into 'master'
|\  
| * 73f5f06 -- Resolve "Update to Angular 12"
|/  
* ccb5710 -- Update project package.json version based on GitLab tag. Done by CI
*   3a7ef81 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * fec892d -- peerDependency update
|/  
* ebc8fdb -- Update project package.json version based on GitLab tag. Done by CI
*   4b15cb4 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 7b4051c -- Update to Angular 11 and dependency update
|/  
*   5feb4cd -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 3ca700e -- recreate package lock
|/  
* 10d0e6b -- Update project package.json version based on GitLab tag. Done by CI
*   be9c86a -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * d4e076f -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   3622129 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 2b504ea -- Resolve "Migrate from tslint to eslint"
|/  
* a7d6ee1 -- Update project package.json version based on GitLab tag. Done by CI
*   ffea373 -- Merge branch '8-update-dependencies-to-new-format' into 'master'
|\  
| * 3d77a3a -- Update dependencies
|/  
* 0d62ecb -- Update project package.json version based on GitLab tag. Done by CI
*   af267dc -- Merge branch '7-rename-to-kypo-user-and-group-api' into 'master'
|\  
| * cad959d -- Rename package
|/  
*   2ec7fdb -- Merge branch '6-use-cypress-image-in-ci' into 'master'
|\  
| * f1a75eb -- Resolve "Use cypress image in CI"
|/  
* 08f0d63 -- Update project package.json version based on GitLab tag. Done by CI
*   3f10dda -- Merge branch '5-refactor-to-use-sentinel-common' into 'master'
|\  
| * 4329cf9 -- Refactor to use @sentinel/common instead of kypo-common
|/  
* 7265560 -- Update project package.json version based on GitLab tag. Done by CI
*   a951a9f -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 98c639d -- Resolve "Update to Angular 10"
|/  
*   36a3ad9 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 167ccc6 -- Update .gitlab-ci.yml
|/  
* 381de6e -- Merge branch '2-add-no-unused-vars-compiler-rule' into 'master'
* 30d1974 -- Add compiler role to throw err on unused local or parameters
### 13.0.1 Add local oidc users api
* 590552f -- [CI/CD] Update packages.json version based on GitLab tag.
*   6bbe54f -- Merge branch '21-add-local-oidc-download-endpoint' into 'master'
|\  
| * f427c5a -- Resolve "Add local OIDC download endpoint"
|/  
* 9934b09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 601d967 -- [CI/CD] Update packages.json version based on GitLab tag.
*   46253f1 -- Merge branch '20-update-to-angular-13' into 'master'
|\  
| * 17e4107 -- Resolve "Update to Angular 13"
|/  
*   61fdbb5 -- Merge branch '19-add-license-file' into 'master'
|\  
| * 7dcacbd -- Add license file
|/  
* deb2bd6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c7ca261 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3b6874d -- Merge branch '18-fix-mapping-of-the-roles-if-roles-are-undefined' into 'master'
|\  
| * 2cdc07f -- Resolve "Fix mapping of the roles if roles are undefined"
|/  
*   4f3d64f -- Merge branch '17-add-node-modules-to-gitignore' into 'master'
|\  
| * 81a14cc -- Add node_modules to gitignore
|/  
*   a8672f5 -- Merge branch '16-bump-version-of-sentinel' into 'master'
|\  
| * 4e99280 -- Bump version of Sentinel
|/  
* 26898fd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 19212ed -- [CI/CD] Update packages.json version based on GitLab tag.
*   f306a5a -- Merge branch '15-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 2d3d127 -- Update gitlab CI
|/  
* a85f65d -- Update project package.json version based on GitLab tag. Done by CI
*   27473c3 -- Merge branch '14-update-to-angular-12' into 'master'
|\  
| * 73f5f06 -- Resolve "Update to Angular 12"
|/  
* ccb5710 -- Update project package.json version based on GitLab tag. Done by CI
*   3a7ef81 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * fec892d -- peerDependency update
|/  
* ebc8fdb -- Update project package.json version based on GitLab tag. Done by CI
*   4b15cb4 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 7b4051c -- Update to Angular 11 and dependency update
|/  
*   5feb4cd -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 3ca700e -- recreate package lock
|/  
* 10d0e6b -- Update project package.json version based on GitLab tag. Done by CI
*   be9c86a -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * d4e076f -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   3622129 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 2b504ea -- Resolve "Migrate from tslint to eslint"
|/  
* a7d6ee1 -- Update project package.json version based on GitLab tag. Done by CI
*   ffea373 -- Merge branch '8-update-dependencies-to-new-format' into 'master'
|\  
| * 3d77a3a -- Update dependencies
|/  
* 0d62ecb -- Update project package.json version based on GitLab tag. Done by CI
*   af267dc -- Merge branch '7-rename-to-kypo-user-and-group-api' into 'master'
|\  
| * cad959d -- Rename package
|/  
*   2ec7fdb -- Merge branch '6-use-cypress-image-in-ci' into 'master'
|\  
| * f1a75eb -- Resolve "Use cypress image in CI"
|/  
* 08f0d63 -- Update project package.json version based on GitLab tag. Done by CI
*   3f10dda -- Merge branch '5-refactor-to-use-sentinel-common' into 'master'
|\  
| * 4329cf9 -- Refactor to use @sentinel/common instead of kypo-common
|/  
* 7265560 -- Update project package.json version based on GitLab tag. Done by CI
*   a951a9f -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 98c639d -- Resolve "Update to Angular 10"
|/  
*   36a3ad9 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 167ccc6 -- Update .gitlab-ci.yml
|/  
* 381de6e -- Merge branch '2-add-no-unused-vars-compiler-rule' into 'master'
* 30d1974 -- Add compiler role to throw err on unused local or parameters
### 13.0.0 Update to Angular 13 and CI/CD optimization.
* 601d967 -- [CI/CD] Update packages.json version based on GitLab tag.
*   46253f1 -- Merge branch '20-update-to-angular-13' into 'master'
|\  
| * 17e4107 -- Resolve "Update to Angular 13"
|/  
*   61fdbb5 -- Merge branch '19-add-license-file' into 'master'
|\  
| * 7dcacbd -- Add license file
|/  
* deb2bd6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* c7ca261 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3b6874d -- Merge branch '18-fix-mapping-of-the-roles-if-roles-are-undefined' into 'master'
|\  
| * 2cdc07f -- Resolve "Fix mapping of the roles if roles are undefined"
|/  
*   4f3d64f -- Merge branch '17-add-node-modules-to-gitignore' into 'master'
|\  
| * 81a14cc -- Add node_modules to gitignore
|/  
*   a8672f5 -- Merge branch '16-bump-version-of-sentinel' into 'master'
|\  
| * 4e99280 -- Bump version of Sentinel
|/  
* 26898fd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 19212ed -- [CI/CD] Update packages.json version based on GitLab tag.
*   f306a5a -- Merge branch '15-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 2d3d127 -- Update gitlab CI
|/  
* a85f65d -- Update project package.json version based on GitLab tag. Done by CI
*   27473c3 -- Merge branch '14-update-to-angular-12' into 'master'
|\  
| * 73f5f06 -- Resolve "Update to Angular 12"
|/  
* ccb5710 -- Update project package.json version based on GitLab tag. Done by CI
*   3a7ef81 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * fec892d -- peerDependency update
|/  
* ebc8fdb -- Update project package.json version based on GitLab tag. Done by CI
*   4b15cb4 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 7b4051c -- Update to Angular 11 and dependency update
|/  
*   5feb4cd -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 3ca700e -- recreate package lock
|/  
* 10d0e6b -- Update project package.json version based on GitLab tag. Done by CI
*   be9c86a -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * d4e076f -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   3622129 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 2b504ea -- Resolve "Migrate from tslint to eslint"
|/  
* a7d6ee1 -- Update project package.json version based on GitLab tag. Done by CI
*   ffea373 -- Merge branch '8-update-dependencies-to-new-format' into 'master'
|\  
| * 3d77a3a -- Update dependencies
|/  
* 0d62ecb -- Update project package.json version based on GitLab tag. Done by CI
*   af267dc -- Merge branch '7-rename-to-kypo-user-and-group-api' into 'master'
|\  
| * cad959d -- Rename package
|/  
*   2ec7fdb -- Merge branch '6-use-cypress-image-in-ci' into 'master'
|\  
| * f1a75eb -- Resolve "Use cypress image in CI"
|/  
* 08f0d63 -- Update project package.json version based on GitLab tag. Done by CI
*   3f10dda -- Merge branch '5-refactor-to-use-sentinel-common' into 'master'
|\  
| * 4329cf9 -- Refactor to use @sentinel/common instead of kypo-common
|/  
* 7265560 -- Update project package.json version based on GitLab tag. Done by CI
*   a951a9f -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 98c639d -- Resolve "Update to Angular 10"
|/  
*   36a3ad9 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 167ccc6 -- Update .gitlab-ci.yml
|/  
* 381de6e -- Merge branch '2-add-no-unused-vars-compiler-rule' into 'master'
* 30d1974 -- Add compiler role to throw err on unused local or parameters
### 12.0.2 Fix mapping of the undefined roles, new version of the Sentinel.
* c7ca261 -- [CI/CD] Update packages.json version based on GitLab tag.
*   3b6874d -- Merge branch '18-fix-mapping-of-the-roles-if-roles-are-undefined' into 'master'
|\  
| * 2cdc07f -- Resolve "Fix mapping of the roles if roles are undefined"
|/  
*   4f3d64f -- Merge branch '17-add-node-modules-to-gitignore' into 'master'
|\  
| * 81a14cc -- Add node_modules to gitignore
|/  
*   a8672f5 -- Merge branch '16-bump-version-of-sentinel' into 'master'
|\  
| * 4e99280 -- Bump version of Sentinel
|/  
* 26898fd -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 19212ed -- [CI/CD] Update packages.json version based on GitLab tag.
*   f306a5a -- Merge branch '15-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 2d3d127 -- Update gitlab CI
|/  
* a85f65d -- Update project package.json version based on GitLab tag. Done by CI
*   27473c3 -- Merge branch '14-update-to-angular-12' into 'master'
|\  
| * 73f5f06 -- Resolve "Update to Angular 12"
|/  
* ccb5710 -- Update project package.json version based on GitLab tag. Done by CI
*   3a7ef81 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * fec892d -- peerDependency update
|/  
* ebc8fdb -- Update project package.json version based on GitLab tag. Done by CI
*   4b15cb4 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 7b4051c -- Update to Angular 11 and dependency update
|/  
*   5feb4cd -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 3ca700e -- recreate package lock
|/  
* 10d0e6b -- Update project package.json version based on GitLab tag. Done by CI
*   be9c86a -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * d4e076f -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   3622129 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 2b504ea -- Resolve "Migrate from tslint to eslint"
|/  
* a7d6ee1 -- Update project package.json version based on GitLab tag. Done by CI
*   ffea373 -- Merge branch '8-update-dependencies-to-new-format' into 'master'
|\  
| * 3d77a3a -- Update dependencies
|/  
* 0d62ecb -- Update project package.json version based on GitLab tag. Done by CI
*   af267dc -- Merge branch '7-rename-to-kypo-user-and-group-api' into 'master'
|\  
| * cad959d -- Rename package
|/  
*   2ec7fdb -- Merge branch '6-use-cypress-image-in-ci' into 'master'
|\  
| * f1a75eb -- Resolve "Use cypress image in CI"
|/  
* 08f0d63 -- Update project package.json version based on GitLab tag. Done by CI
*   3f10dda -- Merge branch '5-refactor-to-use-sentinel-common' into 'master'
|\  
| * 4329cf9 -- Refactor to use @sentinel/common instead of kypo-common
|/  
* 7265560 -- Update project package.json version based on GitLab tag. Done by CI
*   a951a9f -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 98c639d -- Resolve "Update to Angular 10"
|/  
*   36a3ad9 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 167ccc6 -- Update .gitlab-ci.yml
|/  
* 381de6e -- Merge branch '2-add-no-unused-vars-compiler-rule' into 'master'
* 30d1974 -- Add compiler role to throw err on unused local or parameters
### 12.0.1 Update gitlab CI
* 19212ed -- [CI/CD] Update packages.json version based on GitLab tag.
*   f306a5a -- Merge branch '15-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 2d3d127 -- Update gitlab CI
|/  
* a85f65d -- Update project package.json version based on GitLab tag. Done by CI
*   27473c3 -- Merge branch '14-update-to-angular-12' into 'master'
|\  
| * 73f5f06 -- Resolve "Update to Angular 12"
|/  
* ccb5710 -- Update project package.json version based on GitLab tag. Done by CI
*   3a7ef81 -- Merge branch '13-update-peerdependencies-to-angular-11' into 'master'
|\  
| * fec892d -- peerDependency update
|/  
* ebc8fdb -- Update project package.json version based on GitLab tag. Done by CI
*   4b15cb4 -- Merge branch '12-update-to-angular-11' into 'master'
|\  
| * 7b4051c -- Update to Angular 11 and dependency update
|/  
*   5feb4cd -- Merge branch '11-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 3ca700e -- recreate package lock
|/  
* 10d0e6b -- Update project package.json version based on GitLab tag. Done by CI
*   be9c86a -- Merge branch '10-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * d4e076f -- Resolve "Rename package scope to muni-kypo-crp"
|/  
*   3622129 -- Merge branch '9-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 2b504ea -- Resolve "Migrate from tslint to eslint"
|/  
* a7d6ee1 -- Update project package.json version based on GitLab tag. Done by CI
*   ffea373 -- Merge branch '8-update-dependencies-to-new-format' into 'master'
|\  
| * 3d77a3a -- Update dependencies
|/  
* 0d62ecb -- Update project package.json version based on GitLab tag. Done by CI
*   af267dc -- Merge branch '7-rename-to-kypo-user-and-group-api' into 'master'
|\  
| * cad959d -- Rename package
|/  
*   2ec7fdb -- Merge branch '6-use-cypress-image-in-ci' into 'master'
|\  
| * f1a75eb -- Resolve "Use cypress image in CI"
|/  
* 08f0d63 -- Update project package.json version based on GitLab tag. Done by CI
*   3f10dda -- Merge branch '5-refactor-to-use-sentinel-common' into 'master'
|\  
| * 4329cf9 -- Refactor to use @sentinel/common instead of kypo-common
|/  
* 7265560 -- Update project package.json version based on GitLab tag. Done by CI
*   a951a9f -- Merge branch '4-update-to-angular-10' into 'master'
|\  
| * 98c639d -- Resolve "Update to Angular 10"
|/  
*   36a3ad9 -- Merge branch '3-make-the-ci-build-stage-build-with-prod-param' into 'master'
|\  
| * 167ccc6 -- Update .gitlab-ci.yml
|/  
* 381de6e -- Merge branch '2-add-no-unused-vars-compiler-rule' into 'master'
* 30d1974 -- Add compiler role to throw err on unused local or parameters
